.POSIX:

VERSION = 1.3.0
PREFIX ?= /usr/local
MANPREFIX ?= ${PREFIX}/share/man

install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	sed -e "s/VERSION/${VERSION}/g" -e "s:/usr/local:${PREFIX}:g" < walogram > ${DESTDIR}${PREFIX}/bin/walogram
	chmod 755 ${DESTDIR}${PREFIX}/bin/walogram
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < walogram.1 > ${DESTDIR}${MANPREFIX}/man1/walogram.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/walogram.1
	mkdir -p ${DESTDIR}${PREFIX}/share/walogram
	cp -f constants.tdesktop-theme ${DESTDIR}${PREFIX}/share/walogram/constants.tdesktop-theme
	chmod 644 ${DESTDIR}${PREFIX}/share/walogram/constants.tdesktop-theme

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/walogram ${DESTDIR}${MANPREFIX}/man1/walogram.1
	rm -rf ${DESTDIR}${PREFIX}/share/walogram

.PHONY: install uninstall
